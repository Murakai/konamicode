﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            var konami = new KonamiCode(() =>
            {
                var window = new SuccessWindow();
                window.ShowDialog();
            });

            konami.OnKeyHit += Konami_OnKeyHit;
            konami.OnSuccess += Konami_OnSuccess;

            this.KeyDown += konami.Window_KeyDown;
        }

        private void Konami_OnSuccess(object sender)
        {
            //MessageBox.Show("TADA!");
        }

        public List<int> SequenceChars = new List<int>
        {
            241,
            241,
            242,
            242,
            239,
            240,
            239,
            240,
            (int)'B',
            (int)'A',
        };

        private void Konami_OnKeyHit(object sender, int indexHit)
        {
            PbProgress.Value = indexHit + 1;

            if (indexHit == -1)
                TbProgress.Text = "";
            else
            {
                if(indexHit >= 8)
                    TbProgress.FontFamily = new FontFamily("Segoe UI Semibold");
                else
                    TbProgress.FontFamily = new FontFamily("Wingdings");
                TbProgress.Text = ((char)(byte)SequenceChars[indexHit]).ToString();
            }
        }
    }
}
