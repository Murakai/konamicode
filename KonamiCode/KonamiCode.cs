﻿using System;
using System.Windows.Input;
using System.Collections.Generic;

public class KonamiCode
{
    public static List<Key> Sequence = new List<Key>
    {
        Key.Up,
        Key.Up,
        Key.Down,
        Key.Down,
        Key.Left,
        Key.Right,
        Key.Left,
        Key.Right,
        Key.B,
        Key.A
    };

    public Action SuccessAction { get; set; }
    public int CurrentIndex { get; private set; } = 0;

    public delegate void KeyHitEventHandler(object sender, int indexHit);
    public event KeyHitEventHandler OnKeyHit = delegate { };

    public delegate void SuccessEventHandler(object sender);
    public event SuccessEventHandler OnSuccess = delegate { };

    public KonamiCode() { }
    public KonamiCode(Action successAction) 
    {
        SuccessAction = successAction;
    }

    public void Window_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Sequence[CurrentIndex])
        {
            if (CurrentIndex < Sequence.Count-1)
            {
                OnKeyHit?.Invoke(sender, CurrentIndex);
                CurrentIndex++;
            }
            else
            {
                OnKeyHit?.Invoke(sender, CurrentIndex);
                OnSuccess?.Invoke(sender);
                SuccessAction?.Invoke();
                CurrentIndex = 0;
            }
        }
        else if (CurrentIndex != 0)
        {
            CurrentIndex = 0;
            OnKeyHit?.Invoke(sender, -1);
        }
    }
}
